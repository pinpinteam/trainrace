﻿using UnityEngine;
using System.Collections;

public class Rotation : MonoBehaviour {

	public bool world;
	public Vector3 rotation;

	void Start() {
	}
	
	// Update is called once per frame
	void LateUpdate () {
		if(world)
			transform.Rotate (rotation*Time.deltaTime*70, Space.World);
		else 
			transform.Rotate (rotation*Time.deltaTime*70, Space.Self);
	}
}
