﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter)), ExecuteInEditMode]
public class RoadMesh : MonoBehaviour
{
	[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
	[SerializeField] private BezierSpline m_spline;
	[SerializeField] private MeshFilter m_meshFilter;
	[SerializeField] private Vector2[] m_shape;
	[SerializeField] private bool m_inverted;
	[SerializeField] private MeshCollider m_meshCollider;
	private bool isDirty = false;
	private float[] m_UVXs;


	private float m_lastStartDist = 0f;
	private float m_lastEndDist = 0f;


	private void Awake ()
	{
		m_UVXs = new float[m_shape.Length];
		m_UVXs[0] = 0f;
		for (int i = 1; i < m_shape.Length; i++)
		{
			m_UVXs[i] = m_UVXs[i - 1] + Vector2.Distance(m_shape[i - 1], m_shape[i]);
		}
		m_spline = GetComponent<BezierSpline>();
		m_meshFilter = GetComponent<MeshFilter>();
		m_spline.onChange -= SetDirty;
		m_spline.onChange += SetDirty;
		m_spline.onReset += RegenerateMesh;
		GenerateMesh(true);
	}

	private void OnDestroy ()
	{
		m_spline.onChange -= SetDirty;
		m_spline.onReset -= RegenerateMesh;
		DestroyImmediate(m_meshFilter.sharedMesh);
	}

	private void OnValidate ()
	{
		m_UVXs = new float[m_shape.Length];
		m_UVXs[0] = 0f;
		for (int i = 1; i < m_shape.Length; i++)
		{
			m_UVXs[i] = m_UVXs[i - 1] + Vector2.Distance(m_shape[i - 1], m_shape[i]);
		}
		GenerateMesh(true);
	}

	private void SetDirty ()
	{
		isDirty = true;
	}

	private void LateUpdate ()
	{
		if (isDirty)
		{
			this.GenerateMesh(false);
			isDirty = false;
		}
	}

	private void RegenerateMesh()
	{
		GenerateMesh(true);
	}

	public void GenerateMesh ( bool regenerate )
	{
		int definition = m_shape.Length;
		if (regenerate)
		{
			float startDist = m_spline.FirstCurveStartDist;
			int pointCount = (int)((m_spline.TotalLength - startDist -0.01f) / precision);
			int vertexCount = (pointCount + 1) * definition;
			int triangleCount = pointCount * 6 * (definition - 1);
			Vector3[] vertices = new Vector3[vertexCount];
			Vector2[] uvs = new Vector2[vertexCount];
			Vector3[] normals = new Vector3[vertexCount];
			int[] triangles = new int[triangleCount];

			for (int i = 0; i <= pointCount; i++)
			{
				float dist = startDist + i * precision;
				Vector3 position = m_spline.GetPointFromDist(dist);
				Vector3 direction = m_spline.GetDirectionFromDist(dist);
				float angle = m_spline.GetAngleFromDist(dist);
				Vector3 right = m_spline.GetNormalFromDist(dist);
				right = Quaternion.AngleAxis(angle, direction) * right;
				Vector3 up = Vector3.Cross(right, direction);
				for (int j = 0; j < definition; j++)
				{
					int index = i * definition + j;
					vertices[index] = position + up * m_shape[j].y + right * m_shape[j].x;
					normals[index] = up;
					uvs[index] = new Vector2(m_UVXs[j], dist);
					if (i < pointCount)
					{
						int baseIndex = (i * (definition - 1) + j) * 6;
						if (j < definition - 1)
						{
							triangles[baseIndex] = index;
							triangles[baseIndex + (m_inverted ? 2 : 1)] = index + 1;
							triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * definition + j;

							triangles[baseIndex + 3] = index + 1;
							triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * definition + j + 1;
							triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * definition + j;
						}
					}
				}

			}
			Mesh mesh = m_meshFilter.sharedMesh;
			if (mesh == null || regenerate)
			{
				mesh = new Mesh();
				mesh.name = gameObject.name;
			}
			mesh.Clear();
			mesh.vertices = vertices;
			mesh.normals = normals;
			mesh.triangles = triangles;
			mesh.uv = uvs;
			mesh.RecalculateBounds();
			m_meshFilter.sharedMesh = mesh;
			if(m_meshCollider) m_meshCollider.sharedMesh = mesh;
		}
		else
		{
			Mesh mesh = m_meshFilter.sharedMesh;
			Vector3[] vertices = mesh.vertices;
			Vector2[] uvs = mesh.uv;
			Vector3[] normals = mesh.normals;
			int[] triangles = mesh.triangles;
			mesh.Clear();

			//remove old point from start
			float startDist = m_spline.FirstCurveStartDist;
			if (m_lastStartDist < startDist)
			{
				int pointToRemoveCount = (int)((startDist - m_lastStartDist) / precision);
				int vertexToRemoveCount = pointToRemoveCount * definition;
				int triangleToRemoveCount = pointToRemoveCount * 6 * (definition - 1);

				Array.Copy(vertices, vertexToRemoveCount, vertices, 0, vertices.Length - vertexToRemoveCount);
				Array.Copy(uvs, vertexToRemoveCount, uvs, 0, uvs.Length - vertexToRemoveCount);
				Array.Copy(normals, vertexToRemoveCount, normals, 0, normals.Length - vertexToRemoveCount);
				Array.Copy(triangles, triangleToRemoveCount, triangles, 0, triangles.Length - triangleToRemoveCount);

				Array.Resize(ref vertices, vertices.Length - vertexToRemoveCount);
				Array.Resize(ref uvs, uvs.Length - vertexToRemoveCount);
				Array.Resize(ref normals, normals.Length - vertexToRemoveCount);
				Array.Resize(ref triangles, triangles.Length - triangleToRemoveCount);

				for (int i = 0; i < triangles.Length; i++)
				{
					triangles[i] = Mathf.Max(triangles[i] - vertexToRemoveCount, 0);
				}
				
			}
			//add new points to end
			float endDist = m_spline.TotalLength;
			if (m_lastEndDist < endDist)
			{
				int pointCount = (int)((m_spline.TotalLength - m_lastEndDist) / precision);
				int vertexCount = (pointCount + 1) * definition;
				int triangleCount = (pointCount + 1) * 6 * (definition - 1);
				int baseIndex = vertices.Length;
				int triangleBaseIndex = triangles.Length;

				Array.Resize(ref vertices, vertices.Length + vertexCount);
				Array.Resize(ref uvs, uvs.Length + vertexCount);
				Array.Resize(ref normals, normals.Length + vertexCount);
				Array.Resize(ref triangles, triangles.Length + triangleCount);
				
				for (int i = 0; i <= pointCount; i++)
				{
					float dist = m_lastEndDist + i * precision;
					Vector3 position = m_spline.GetPointFromDist(dist);
					Vector3 direction = m_spline.GetDirectionFromDist(dist);
					float angle = m_spline.GetAngleFromDist(dist);
					Vector3 right = m_spline.GetNormalFromDist(dist);
					right = Quaternion.AngleAxis(angle, direction) * right;
					Vector3 up = Vector3.Cross(right, direction);
					for (int j = 0; j < definition; j++)
					{
						int index = baseIndex + i * definition + j;
						vertices[index] = position + up * m_shape[j].y + right * m_shape[j].x;
						normals[index] = up;
						uvs[index] = new Vector2(m_UVXs[j], dist);
						int triangleIndex = triangleBaseIndex + (i * (definition - 1) + j) * 6;
						if (j < definition - 1)
						{
							triangles[triangleIndex] = baseIndex + (i - 1) * definition + j;
							triangles[triangleIndex + (m_inverted ? 2 : 1)] = baseIndex + (i - 1) * definition + j + 1;
							triangles[triangleIndex + (m_inverted ? 1 : 2)] = index;

							triangles[triangleIndex + 3] = baseIndex + (i - 1) * definition + j + 1;
							triangles[triangleIndex + (m_inverted ? 5 : 4)] = index + 1;
							triangles[triangleIndex + (m_inverted ? 4 : 5)] = index;
						}
					}
				}
			}
			mesh.vertices = vertices;
			mesh.normals = normals;
			mesh.triangles = triangles;
			mesh.uv = uvs;
			mesh.RecalculateBounds();
		}

		m_lastStartDist = m_spline.FirstCurveStartDist;
		m_lastEndDist = m_spline.TotalLength;

	}

}
