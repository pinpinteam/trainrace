﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter)), ExecuteInEditMode]
public class TubeMesh : MonoBehaviour
{
	[SerializeField, Range(0.01f, 1f)] private float precision = 1f;
	[SerializeField, Range(2, 100)] private int definition = 8;
	[SerializeField] private float m_radius = 1f;
	[SerializeField] private BezierSpline m_spline;
	[SerializeField] private MeshFilter m_meshFilter;
	[SerializeField] private bool m_inverted;
	private bool isDirty = false;

	private float m_lastStartDist = 0f;
	private float m_lastEndDist = 0f;

	private void Awake ()
	{
		m_spline = GetComponent<BezierSpline>();
		m_meshFilter = GetComponent<MeshFilter>();
		m_spline.onChange -= SetDirty;
		m_spline.onChange += SetDirty;
		m_spline.onReset += RegenerateMesh;
	}

	private void OnDestroy ()
	{
		m_spline.onChange -= SetDirty;
		m_spline.onReset -= RegenerateMesh;
		DestroyImmediate(m_meshFilter.sharedMesh);
	}

	private void OnValidate ()
	{
		GenerateMesh(true);
	}

	private void SetDirty ()
	{
		isDirty = true;
	}

	private void LateUpdate ()
	{
		if (isDirty)
		{
			this.GenerateMesh(false);
			isDirty = false;
		}
	}

	private void RegenerateMesh ()
	{
		GenerateMesh(true);
	}

	public void GenerateMesh ( bool regenerte )
	{
		int definition = this.definition + 1;
		if (regenerte)
		{
			float totalLenght = m_spline.TotalLength + 1;
			int pointCount = (int)(totalLenght / precision);
			int vertexCount = (pointCount + 1) * definition;
			int triangleCount = pointCount * 6 * (definition - 1);
			Vector3[] vertices = new Vector3[vertexCount];
			Vector2[] uvs = new Vector2[vertexCount];
			int[] triangles = new int[triangleCount];
			Vector3 lastNormal = Vector3.up;

			for (int i = 0; i <= pointCount; i++)
			{
				float dist = i * precision;
				Vector3 position = m_spline.GetPointFromDist(dist);
				Vector3 direction = m_spline.GetDirectionFromDist(dist);
				float angle = m_spline.GetAngleFromDist(dist);
				Vector3 right = m_spline.GetNormalFromDist(dist);
				right = Quaternion.AngleAxis(angle, direction) * right;
				for (int j = 0; j < definition; j++)
				{
					int index = i * definition + j;
					vertices[index] = position + (Quaternion.AngleAxis((180f / (definition - 1) - 90f) + 360f / (definition - 1) * j, direction) * right) * m_radius;
					uvs[index] = new Vector2((float)j / (definition - 1), dist);
					if (i < pointCount)
					{
						int baseIndex = (i * (definition - 1) + j) * 6;
						if (j < definition - 1)
						{
							triangles[baseIndex] = i * definition + j;
							triangles[baseIndex + (m_inverted ? 2 : 1)] = i * definition + j + 1;
							triangles[baseIndex + (m_inverted ? 1 : 2)] = (i + 1) * definition + j;

							triangles[baseIndex + 3] = i * definition + j + 1;
							triangles[baseIndex + (m_inverted ? 5 : 4)] = (i + 1) * definition + j + 1;
							triangles[baseIndex + (m_inverted ? 4 : 5)] = (i + 1) * definition + j;
						}
					}
				}

			}
			Mesh mesh = m_meshFilter.sharedMesh;
			if (mesh == null)
			{
				mesh = new Mesh();
				mesh.name = gameObject.name;
			}
			mesh.Clear();
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.uv = uvs;
			mesh.RecalculateNormals();
			mesh.RecalculateBounds();
			m_meshFilter.sharedMesh = mesh;
		}
		else
		{
			Mesh mesh = m_meshFilter.sharedMesh;
			Vector3[] vertices = mesh.vertices;
			Vector2[] uvs = mesh.uv;
			int[] triangles = mesh.triangles;
			mesh.Clear();

			//remove old point from start
			float startDist = m_spline.FirstCurveStartDist;
			if (m_lastStartDist < startDist)
			{
				int pointToRemoveCount = (int)((startDist - m_lastStartDist) / precision);
				int vertexToRemoveCount = pointToRemoveCount * definition;
				int triangleToRemoveCount = pointToRemoveCount * 6 * (definition - 1);

				Array.Copy(vertices, vertexToRemoveCount, vertices, 0, vertices.Length - vertexToRemoveCount);
				Array.Copy(uvs, vertexToRemoveCount, uvs, 0, uvs.Length - vertexToRemoveCount);
				Array.Copy(triangles, triangleToRemoveCount, triangles, 0, triangles.Length - triangleToRemoveCount);

				Array.Resize(ref vertices, vertices.Length - vertexToRemoveCount);
				Array.Resize(ref uvs, uvs.Length - vertexToRemoveCount);
				Array.Resize(ref triangles, triangles.Length - triangleToRemoveCount);

				for (int i = 0; i < triangles.Length; i++)
				{
					triangles[i] = Mathf.Max(triangles[i] - vertexToRemoveCount, 0);
				}

			}
			//add new points to end
			float endDist = m_spline.TotalLength;
			if (m_lastEndDist < endDist)
			{
				int pointCount = (int)((m_spline.TotalLength - m_lastEndDist) / precision);
				int vertexCount = (pointCount + 1) * definition;
				int triangleCount = (pointCount + 1) * 6 * (definition - 1);
				int baseIndex = vertices.Length;
				int triangleBaseIndex = triangles.Length;

				Array.Resize(ref vertices, vertices.Length + vertexCount);
				Array.Resize(ref uvs, uvs.Length + vertexCount);
				Array.Resize(ref triangles, triangles.Length + triangleCount);
				
				for (int i = 0; i <= pointCount; i++)
				{
					float dist = m_lastEndDist + i * precision;
					Vector3 position = m_spline.GetPointFromDist(dist);
					Vector3 direction = m_spline.GetDirectionFromDist(dist);
					float angle = m_spline.GetAngleFromDist(dist);
					Vector3 right = m_spline.GetNormalFromDist(dist);
					right = Quaternion.AngleAxis(angle, direction) * right;
					for (int j = 0; j < definition; j++)
					{
						int index = baseIndex + i * definition + j;
						vertices[index] = position + (Quaternion.AngleAxis((180f / (definition - 1) - 90f) + 360f / (definition - 1) * j, direction) * right) * m_radius;
						uvs[index] = new Vector2((float)j / (definition - 1), dist);
						int triangleIndex = triangleBaseIndex + (i * (definition - 1) + j) * 6;
						if (j < definition - 1)
						{
							triangles[triangleIndex] = baseIndex + (i - 1) * definition + j;
							triangles[triangleIndex + (m_inverted ? 2 : 1)] = baseIndex + (i - 1) * definition + j + 1;
							triangles[triangleIndex + (m_inverted ? 1 : 2)] = index;

							triangles[triangleIndex + 3] = baseIndex + (i - 1) * definition + j + 1;
							triangles[triangleIndex + (m_inverted ? 5 : 4)] = index + 1;
							triangles[triangleIndex + (m_inverted ? 4 : 5)] = index;
						}
					}
				}
			}
			mesh.vertices = vertices;
			mesh.triangles = triangles;
			mesh.uv = uvs;
			mesh.RecalculateBounds();
		}

		m_lastStartDist = m_spline.FirstCurveStartDist;
		m_lastEndDist = m_spline.TotalLength;


	}

}
