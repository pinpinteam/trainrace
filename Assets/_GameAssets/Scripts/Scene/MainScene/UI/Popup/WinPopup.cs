﻿using UnityEngine;
using Pinpin.UI;
using System;

namespace Pinpin.Scene.MainScene.UI
{

	public sealed class WinPopup: AClosablePopup
	{
        public static event Action OnClickPopup;

		private new MainSceneUIManager UIManager
		{
			get { return (base.UIManager as MainSceneUIManager); }
		}

		protected override void OnClose()
		{
            OnClickPopup?.Invoke();
            base.OnClose();
		}

	}

}