﻿using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
namespace Pinpin
{

	public class ProgressionBar : MonoBehaviour
	{

		[SerializeField] Image m_emptyDotPrefab;
		[SerializeField] Image m_completeDotPrefab;

		[SerializeField] HorizontalLayoutGroup m_emptyDotsLayout;
		[SerializeField] HorizontalLayoutGroup m_completeDotsLayout;
		[SerializeField] private Image m_progressionGauge;

		List<Image> m_emptyRoundPoints = new List<Image>();
		List<Image> m_completeRoundPoints = new List<Image>();
		int m_roundCount = 0;
		List<float> m_progressionStates = new List<float>();
		private RectTransform m_rectTransform;

		private void Awake ()
		{
			m_rectTransform = (RectTransform)transform;
			GameManager.OnUpdateProgression += UpdateProgression;
		}

		public void UpdateProgression(int round, float value, int maxRound)
		{
			Setup(Mathf.Max(0, maxRound - 1));
			float finalValue = m_progressionStates[round] + (m_progressionStates[round + 1] - m_progressionStates[round]) * value;
			m_progressionGauge.DOFillAmount(finalValue, 0.1f);

		}

		public void Setup (int roundCount = 1)
		{
			if (roundCount > m_roundCount)
			{
				for (int i = m_roundCount; i < roundCount; i++)
				{
					Image newEmptyDot = Instantiate(m_emptyDotPrefab, m_emptyDotsLayout.transform);
					newEmptyDot.gameObject.SetActive(true);
					m_emptyRoundPoints.Add(newEmptyDot);
					Image newCompleteDot = Instantiate(m_completeDotPrefab, m_completeDotsLayout.transform);
					newCompleteDot.gameObject.SetActive(true);
					m_completeRoundPoints.Add(newCompleteDot);
				}
			}
			else if (roundCount < m_roundCount)
			{
				for (int i = m_roundCount; i > roundCount; i--)
				{
					if (i - 1 < m_emptyRoundPoints.Count)
					{
						Destroy(m_emptyRoundPoints[i - 1].gameObject);
						m_emptyRoundPoints.RemoveAt(i - 1);
						
						Destroy(m_completeRoundPoints[i - 1].gameObject);
						m_completeRoundPoints.RemoveAt(i - 1);
					}
				}
			}
			m_roundCount = roundCount;
			if (m_roundCount > 0)
			{
				float dotWidth = ((RectTransform)m_emptyDotsLayout.transform).rect.height;
				float spacing = (((RectTransform)m_emptyDotsLayout.transform).rect.width - (m_roundCount * dotWidth)) / (m_roundCount + 1);
				m_emptyDotsLayout.spacing = spacing;
				m_completeDotsLayout.spacing = spacing;
				float totalWidth = m_rectTransform.rect.width;
				m_progressionStates.Clear();
				m_progressionStates.Add(m_rectTransform.rect.height / totalWidth);

				for (int i = 0; i < m_roundCount; i++)
				{
					m_progressionStates.Add((m_rectTransform.rect.height + ((i + 1) * (spacing + dotWidth))) / totalWidth);
				}
				m_progressionStates.Add(1f);
			}
			LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)m_emptyDotsLayout.transform);
			LayoutRebuilder.ForceRebuildLayoutImmediate((RectTransform)m_completeDotsLayout.transform);
		}

		

	}

}