﻿using UnityEngine;

namespace Pinpin
{
	public class FXManager : MonoBehaviour
	{
		[SerializeField] private ParticleSystem m_moneyPS;
		public static FXManager Instance;

		private void Awake()
		{
			if (Instance == null)
				Instance = this;
			else
				Destroy(gameObject);
		}

		public void PlayMoney(bool isRewarded = false)
		{
			ParticleSystem.Burst[] bursts;

			if (isRewarded)
			{
				bursts = new ParticleSystem.Burst[2];
				bursts[1].time = 0.25f;
				bursts[1].count = 20;
				bursts[1].cycleCount = 8;
				bursts[1].repeatInterval = 0.05f;
				bursts[0].time = 0f;
				bursts[0].count = 20;
				bursts[0].cycleCount = 8;
				bursts[0].repeatInterval = 0.05f;
			}
			else
			{
				bursts = new ParticleSystem.Burst[1];
				bursts[0].time = 0f;
				bursts[0].count = 10;
				bursts[0].cycleCount = 8;
				bursts[0].repeatInterval = 0.05f;
			}
			m_moneyPS.emission.SetBursts(bursts);
			m_moneyPS.Play();
		}
	}
}