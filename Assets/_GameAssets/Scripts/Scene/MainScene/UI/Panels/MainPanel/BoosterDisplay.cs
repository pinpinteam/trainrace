﻿using DG.Tweening;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.Scene.MainScene.UI
{
	public class BoosterDisplay : MonoBehaviour
	{
		[SerializeField] private Image m_timerImage;
		[SerializeField] private TextMeshProUGUI m_timerText;
		[SerializeField] private Boosters.BoosterType m_boosterType;
		[SerializeField] private bool m_isLimited;
		[SerializeField] Image m_progressRing;

		private float m_boosterDuration;
		private float m_actualBoosterTimer;
		private Sequence m_sequence;

		public static Action onLimitedBoosterDisplayExpire;

		private void Awake ()
		{
			if (m_isLimited)
			{
				MainPanel.OnDisplayLimitedBooster += OnDisplayLimitedBooster;
				gameObject.SetActive(false);
			}
		}

		// Use this for initialization
		void Start ()
		{
			if (!m_isLimited)
			{
				m_boosterDuration = ApplicationManager.config.game.boosterTimer[(int)m_boosterType];
				GameDatas.OnBoosterTimeIncreased += UpdateMaxBoosterTime;
				UpdateMaxBoosterTime(m_boosterType);
			}
		}

		private void OnDisplayLimitedBooster ( Boosters.BoosterType boosterType )
		{
			if (boosterType == m_boosterType)
			{
				m_boosterDuration = ApplicationManager.config.game.limitedBoosterTimer;
				m_actualBoosterTimer = m_boosterDuration;
				gameObject.SetActive(true);
				m_sequence = DOTween.Sequence();

				//				m_sequence.Append(transform.DOScale(0f, 0.2f).SetEase(Ease.Linear));
				m_sequence.Append(transform.DOScale(2.00f, 0.2f).SetEase(Ease.Linear));
				m_sequence.Append(transform.DOScale(1f, 0.1f).SetEase(Ease.Linear));
			}
		}

		// Update is called once per frame
		void Update ()
		{
			if (m_isLimited)
			{
				m_actualBoosterTimer -= Time.deltaTime;
				//m_timerText.text = FloatToTimeString(m_actualBoosterTimer);
				m_progressRing.fillAmount =1- (m_boosterDuration - m_actualBoosterTimer) / m_boosterDuration;
				if (m_actualBoosterTimer <= 0)
				{
					gameObject.SetActive(false);
					if (onLimitedBoosterDisplayExpire != null)
						onLimitedBoosterDisplayExpire.Invoke();
				}
				return;
			}
			if (ApplicationManager.datas.IsBoosterActive(m_boosterType))
			{
				m_actualBoosterTimer = ApplicationManager.datas.BoosterRemainingTime(m_boosterType);
				m_timerImage.fillAmount = m_actualBoosterTimer / m_boosterDuration;
				m_timerText.text = FloatToTimeString(m_actualBoosterTimer);
			}
			else
			{
				gameObject.SetActive(false);
			}
		}

		private void UpdateMaxBoosterTime ( Boosters.BoosterType type )
		{
			if (m_boosterType == type)
			{
				gameObject.SetActive(true);
				m_boosterDuration = ApplicationManager.datas.BoosterRemainingTime(type);
			}
		}

		private string FloatToTimeString ( float timer )
		{
			string minutes = Mathf.Floor(timer / 60).ToString("00");
			string seconds = (timer % 60).ToString("00");

			return (minutes + ":" + seconds);
		}

		private void OnDestroy ()
		{
			if (!m_isLimited)
			{
				GameDatas.OnBoosterTimeIncreased -= UpdateMaxBoosterTime;
			}
			else
			{
				MainPanel.OnDisplayLimitedBooster -= OnDisplayLimitedBooster;
			}
		}
	}
}
