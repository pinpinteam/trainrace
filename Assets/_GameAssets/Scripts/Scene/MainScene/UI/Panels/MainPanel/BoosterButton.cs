﻿using System;
using System.Numerics;
using UnityEngine;

namespace Pinpin.UI
{
	public class BoosterButton : MonoBehaviour
	{

		public static Action<Boosters.BoosterType, bool> onClick;
		[SerializeField] private PushButton button;
		[SerializeField] private Boosters.BoosterType upgradeType;
		[SerializeField] private bool m_limitedBonus;

		private BigInteger m_cost;
		private int m_level;

		void Start ()
		{
			button.onClick += OnButtonClick;
			ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
#if MADPINPIN_ADS
			button.isInteractable = ApplicationManager.canWatchRewardedVideo;
#endif
		}

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{
			button.isInteractable = value;
		}

		private void OnDestroy ()
		{
			button.onClick -= OnButtonClick;
			ApplicationManager.onRewardedVideoAvailabilityChange -= OnRewardedVideoAvailabilityChange;
		}

		private void OnButtonClick ()
		{
			if (onClick != null)
				onClick.Invoke(upgradeType, m_limitedBonus);
			if (m_limitedBonus)
				gameObject.SetActive(false);
		}

	}

}