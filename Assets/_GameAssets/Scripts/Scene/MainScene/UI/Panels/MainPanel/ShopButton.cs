﻿using Pinpin.Helpers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin.UI
{

	public class ShopButton : MonoBehaviour
	{

		public static Action<string> onClick;
		[SerializeField] private PushButton button;
		[SerializeField] private Text m_levelText;
		[SerializeField] private string id;

		private BigInteger m_cost;
		private int m_level;

		void Start ()
		{
			button.onClick += OnButtonClick;
			
		}

		private void OnDestroy ()
		{
			button.onClick -= OnButtonClick;
		}

		private void OnButtonClick ()
		{
			if (onClick != null)
				onClick.Invoke(id);
		}

	}

}