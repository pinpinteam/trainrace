﻿using UnityEngine;
using Pinpin.Scene;
using GameScene = Pinpin.Scene.ASceneManager.Scene;
using Pinpin.UI;
using System.Collections;
#if UNITY_WEBGL && !UNITY_EDITOR
	using System.Runtime.InteropServices;
#endif

namespace Pinpin.InitScene
{

	/*
		Do Platform Dependant Initialization here.
	*/
	public sealed class InitSceneManager: ASceneManager
	{
		
		#if UNITY_WEBGL && !UNITY_EDITOR
			[DllImport("__Internal")]
			private static extern void HideLoadingScreen();
		#endif
		[SerializeField] private GameScene m_sceneToLoadAfterInit;

		protected override void Awake ()
		{
			base.Awake();
			#if UNITY_WEBGL && !UNITY_EDITOR
				HideLoadingScreen();
			#endif
		}

		public void ContinueLoading ()
		{
			StartCoroutine(LoadSceneAfterDelay(0.01f));
		}




		private IEnumerator LoadSceneAfterDelay(float delay)
		{
			yield return new WaitForSeconds(delay);
				m_sceneToLoadAfterInit = GameScene.MainScene;

			ApplicationManager.LoadScene(m_sceneToLoadAfterInit);
		}

		protected override void Start ()
		{
			base.Start();
		}
		
	}

}