using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
namespace Pinpin.UI {
	[ExecuteInEditMode]
	public class UIGaugeMarker : UIGaugeObject
	{

		[SerializeField] private float m_distance;

		public float distance
		{
			get { return m_getter != null ? m_getter() : m_distance; }

			set
			{
				if (m_getter != null)
					Debug.LogWarning(string.Format("UIGaugeMarker - Trying to change 'distance' field of '{0}' but this filed is marked as tracked", gameObject.name));
				else
					m_distance = value;
			}
		}

		public override void Init()
		{
			base.Init();

			Vector2 anchors;

			anchors = m_rect.anchorMax;
			anchors[(int)m_compass.axis] = 0;

			m_rect.anchorMax = anchors;
			m_rect.anchorMin = anchors;
		}

		protected override void Update()
		{
			base.Update();

			if (m_compass != null && m_rect != null)
			{
				float distRatio;
				Vector3 position;

				position = m_rect.anchoredPosition;
				//position = transform.localPosition;
				distRatio = (distance - m_compass.min) / m_compass.max;
				m_ratio = m_compass.direction == UIGauge.EDirection.Invert ? 1 - distRatio : distRatio;
				position[(int)m_compass.axis] = m_compass.size * Mathf.Clamp01(m_ratio);

				m_rect.anchoredPosition = position;
			}

		}
	}
	
}
