﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using TMPro;
namespace Pinpin.UI
{

	[DisallowMultipleComponent, RequireComponent(typeof(TextMeshProUGUI))]
	public class UITMProCurrencyUpdater : MonoBehaviour
	{

		public float animDuration = 1f;
		
		public event UnityAction onAnimationEndEvent;

		private int lastDisplayedValue { get; set; }
		private int toDisplayValue { get; set; }
		private int currentDisplayedValue { get; set; }

		[SerializeField] private bool m_animateOnEnable = true;

		private TextMeshProUGUI field { get; set; }
		private int currentDisplayed { get; set; }
		private YieldInstruction waitFrame { get; set; }
		private Coroutine currentCoroutine { get; set; }

		public void UpdateValue ( int from, int to, bool animate = true )
		{
			lastDisplayedValue = from;
			toDisplayValue = to;
			
			if (!animate)
				currentDisplayedValue = toDisplayValue;
		}
		
		public void TriggerUpdate (float duration)
		{
			animDuration = duration;
			UpdateValue();
		}

		private void Awake ()
		{
			this.field = this.GetComponent<TextMeshProUGUI>();
			this.waitFrame = new WaitForEndOfFrame();
		}

		private void OnEnable ()
		{
			if (currentDisplayedValue != toDisplayValue && m_animateOnEnable)
				this.UpdateValue();
			else
				this.SetAmount(toDisplayValue);
		}

		private void SetAmount ( int amount )
		{
			this.field.text = amount.ToString("n0", ApplicationManager.currentCulture.NumberFormat);
			this.currentDisplayed = amount;
			currentDisplayedValue = amount;
		}

		private void UpdateValue ()
		{
			if (this.isActiveAndEnabled == false)
				return ;
				
			if (currentDisplayedValue == toDisplayValue)
				return ;
				
			if (this.currentCoroutine == null)
				this.currentDisplayed = lastDisplayedValue;
			else
				this.StopCoroutine(this.currentCoroutine);
				
			int diff = toDisplayValue - this.currentDisplayed;
			if (diff == 0)
			{
				this.SetAmount(toDisplayValue);
				return;
			}

			this.currentCoroutine = this.StartCoroutine(this.AnimateValue(diff, animDuration));
		}
		
		private IEnumerator AnimateValue ( int diffValue, float speed )
		{
			float currentFloatValue = this.currentDisplayed;
			
			while (this.isActiveAndEnabled)
			{
				if (diffValue < 0) // - value
				{
					while ( this.currentDisplayed > toDisplayValue )
					{ 
						yield return ( this.waitFrame );
						currentFloatValue += (float)diffValue / speed * Time.deltaTime;
						if (this.currentDisplayed != (int)currentFloatValue)
							this.SetAmount((int)currentFloatValue);
					}
				}
				else // + value
				{
					while ( this.currentDisplayed < toDisplayValue )
					{ 
						yield return ( this.waitFrame );
						currentFloatValue += (float)diffValue / speed * Time.deltaTime;
						if (this.currentDisplayed != (int)currentFloatValue)
							this.SetAmount((int)currentFloatValue);
					}
				}
				this.SetAmount(toDisplayValue);
				break;
			}

			if (onAnimationEndEvent != null)
				onAnimationEndEvent.Invoke();

			this.currentCoroutine = null;
		}

	}

}