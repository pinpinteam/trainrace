﻿using Pinpin;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UISounds : MonoBehaviour
{

	private static UISounds singleton;
	[SerializeField] private AudioSource m_clicSound;

	private void Start ()
	{
		if (singleton == null)
			singleton = this;
		else
			Destroy(this.gameObject);
	}

	public static void PlayClickSound ()
	{
		if (singleton != null)
			singleton.m_clicSound.Play();
	}

}
