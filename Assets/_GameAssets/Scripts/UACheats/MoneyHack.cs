﻿using System.Numerics;
using UnityEngine;
using UnityEngine.UI;

namespace Pinpin
{
	public class MoneyHack : MonoBehaviour
	{
		[SerializeField] private Button m_button;

		// Use this for initialization
		void Start()
		{
			m_button.onClick.AddListener(HideUIAction);
		}

		private void Update()
		{
			if (Input.GetKey(KeyCode.U))
			{
				HideUIAction();
			}
		}

		private void HideUIAction()
		{
			GameManager.Instance.AddMillion();
		}
	}
}
