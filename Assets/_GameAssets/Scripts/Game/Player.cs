﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public delegate void OnPlayerAccelerateEventHandler(float speed);
	public class Player : Character
	{
		public static event Action OnPlayerCrash;
		public static event Action OnPlayerFinish;
		public static event OnPlayerAccelerateEventHandler OnPlayerAccelerate;

        
        [SerializeField] public Transform cameraTransform;
		[SerializeField] private Transform body;
		[SerializeField] private ParticleSystem leftSparkParticle;
		[SerializeField] private ParticleSystem rightSparkParticle;
		[SerializeField] private float m_cameraDistance;
        [SerializeField] private float m_angleToFOVMultiplier;
		[SerializeField] private float m_maxVerticalForce; 
		[SerializeField] private float m_maxCentrifugalForce;
		[SerializeField] private float m_maxUpwardsForce;
		[SerializeField, Range(0, 1)] private float m_maxVerticalForceAttenuation;
        [SerializeField] private float m_maxUnstableRotation;
        [SerializeField] private float m_unstableMovementDuration;
        [SerializeField, Range(0, 1)] private float m_unstabilityThreshold;
        [SerializeField] private float m_crashCentrifugalMultiplier;
        [SerializeField] private float m_crashUpwardsMultiplier;
        [SerializeField] private float m_crashVerticalMultiplier;
        [SerializeField, Range(0, 1)] private float m_crashTorqueWeight;
        [SerializeField, Range(0, 1)] private float m_crashCentrifugalAngleWeight;
        [SerializeField, Range(0, 1)] private float m_crashUpwardsAngleWeight;
        [SerializeField, Range(0, 1)] private float m_crashVerticalAngleWeight;
        [SerializeField] private float m_loopingSafetyValue = 25;
        [SerializeField] private float m_deathDelay = 1;

        

        private Vector3 m_cameraPosition;
		private Rigidbody m_rigidbody;
        private bool IsUnstable = false;
        private Quaternion m_baseRotation;
        private Quaternion m_currentUnstableRotation;
        private Quaternion m_currentStableRotation;
        private Quaternion m_unstableRotation;
        private float m_unstableTime = 0;
        private float m_stableTime = 0;
        private float m_currentMovementDuration = 0;
        private float m_deathCount = 0;

        //private Vector3 temp;





        private void Awake()
		{
			m_rigidbody = GetComponent<Rigidbody>();
            IsUnstable = false;
            m_baseRotation = body.localRotation;
            m_currentStableRotation = m_baseRotation;

            //temp = body.forward;
        }

		private void CheckInput()
		{
            /*if(Input.GetMouseButtonDown(1)) temp *= -1;
            if (Input.GetMouseButton(1))
            {
                IsUnstable = true;
                m_unstableRotation = m_baseRotation * Quaternion.AngleAxis(m_maxUnstableRotation, temp);
                
            }
            else IsUnstable = false;*/

            if (Input.GetMouseButton(0))
				Accelerate();
            else
            {
                if (m_deathCount > 0) m_deathCount -= Time.deltaTime * 2;
                else m_deathCount = 0;

                if (m_speed > m_minSpeed)
                    Decelerate();
            }
			
		}

		protected override void DoActionNormal()
		{
			CheckInput();

			base.DoActionNormal();

			m_cameraPosition = m_level.GetPointInWorldFromDist(distance + m_cameraDistance);
			cameraTransform.position = m_cameraPosition;


			float horizontalAngle = GetHorizontalAngle();
			float verticalAngle = GetVerticallAngle();
			float levelAngle = m_level.GetAngleFromDist(distance);

            levelAngle = levelAngle % 90;
			float verticalForce = levelAngle * Mathf.Max((1 - m_speed / m_maxSpeed), m_maxVerticalForceAttenuation);
            float centrifugalForce = horizontalAngle * m_speed;
            float upwardsForce = -verticalAngle * m_speed;

            Vector3 centrifugalForceDirection = Quaternion.Euler(0, -centrifugalForce * m_crashCentrifugalAngleWeight, 0) * transform.forward * m_crashCentrifugalMultiplier;
            Vector3 upwardsForceDirection = transform.forward * m_crashUpwardsMultiplier;
            Vector3 verticalForceDirection = Quaternion.Euler(0, 0, -verticalForce * m_crashVerticalAngleWeight) * transform.right * m_crashVerticalMultiplier;

			if (Mathf.Abs(verticalForce + horizontalAngle) > m_maxVerticalForce)
			{
                m_deathCount += Time.deltaTime;
                if(m_deathCount > m_deathDelay && Mathf.Abs(transform.rotation.z * Mathf.Rad2Deg) > m_loopingSafetyValue) SetModeCrash(verticalForceDirection);
			}
			else if (Mathf.Abs(centrifugalForce + levelAngle) > m_maxCentrifugalForce)
			{
                m_deathCount += Time.deltaTime;
                if (m_deathCount > m_deathDelay && !(Mathf.Abs(centrifugalForce) - Mathf.Abs(levelAngle) < 0))
                {
                    SetModeCrash(centrifugalForceDirection);
                }
				
			}
			else if (upwardsForce > m_maxUpwardsForce)
			{
                m_deathCount += Time.deltaTime;
                if (m_deathCount > m_deathDelay) SetModeCrash(upwardsForceDirection);


            }
			else if (Mathf.Abs(centrifugalForce + levelAngle) > m_unstabilityThreshold * m_maxCentrifugalForce)
			{
				IsUnstable = true;
                if (m_deathCount > 0) m_deathCount -= Time.deltaTime;
                else m_deathCount = 0;
                m_unstableRotation = m_baseRotation * Quaternion.AngleAxis(m_maxUnstableRotation, -Vector3.forward * horizontalAngle);

			}
			else if (upwardsForce > m_unstabilityThreshold * m_maxUpwardsForce)
			{
				IsUnstable = true;
                if (m_deathCount > 0) m_deathCount -= Time.deltaTime;
                else m_deathCount = 0;
                m_unstableRotation = m_baseRotation;
			}
			else if (Mathf.Abs(verticalForce + horizontalAngle) > m_unstabilityThreshold * m_maxVerticalForce)
			{
				IsUnstable = true;
                if (m_deathCount > 0) m_deathCount -= Time.deltaTime;
                else m_deathCount = 0;
                m_unstableRotation = m_baseRotation * Quaternion.AngleAxis(m_maxUnstableRotation, -Vector3.forward * levelAngle);
			}
			else
			{
                if (m_deathCount > 0) m_deathCount -= Time.deltaTime;
                else m_deathCount = 0;
                IsUnstable = false;
			}

            if (distance >= m_level.TotalLength)
			{
                OnPlayerFinish?.Invoke();
                distance = 0;
                IsUnstable = false;
                SetModeEnd();
                leftSparkParticle.Stop();
                rightSparkParticle.Stop();
            }
			OnPlayerAccelerate?.Invoke(m_speed - m_minSpeed + Mathf.Abs(horizontalAngle) + Mathf.Abs(verticalAngle) * m_angleToFOVMultiplier);

            float ratio;
            if(!IsUnstable)
            {

				if (m_unstableTime > 0)
				{
					ratio = 1 - m_unstableTime / m_currentMovementDuration;
					body.localRotation = Quaternion.Lerp(m_currentUnstableRotation, m_baseRotation, ratio);
					m_unstableTime -= Time.deltaTime;
				}
				else
				{
					m_unstableTime = 0;
					leftSparkParticle.Stop();
					rightSparkParticle.Stop();
				}

                m_currentStableRotation = body.localRotation;
                m_stableTime = m_unstableTime;

            }
            else
            {

                if (m_unstableTime - m_stableTime < m_unstableMovementDuration - m_stableTime)
                {
                    ratio = (m_unstableTime - m_stableTime) / (m_unstableMovementDuration - m_stableTime);
                    body.localRotation = Quaternion.Lerp(m_currentStableRotation, m_unstableRotation, ratio);
                    m_unstableTime += Time.deltaTime;
                }
                else m_unstableTime = m_unstableMovementDuration;

				if (body.localRotation.x < 0)
					leftSparkParticle.Play();
				else if (body.localRotation.x < 0)
                    rightSparkParticle.Play();
                else
                {
                    leftSparkParticle.Play();
                    rightSparkParticle.Play();
                }

                m_currentUnstableRotation = body.localRotation;
                m_currentMovementDuration = m_unstableTime;
            }

        }


        private float GetHorizontalAngle()
		{
			Vector3 velocity = m_level.GetVelocityFromDist(distance);
			Vector3 futurVelocity = m_level.GetVelocityFromDist(distance + 1);
			Vector3 verticalVelocity = velocity;

			float z = Vector3.Dot(futurVelocity, transform.forward);
			float x = Vector3.Dot(futurVelocity, transform.right);


			float angle = Mathf.Atan2(x, z) * Mathf.Rad2Deg;
			return angle;

            //return Vector3.SignedAngle(transform.position + transform.forward, horizontalVectorToCompareAngle, transform.up);
		}

		private float GetVerticallAngle()
		{
			Vector3 velocity = m_level.GetVelocityFromDist(distance);
			Vector3 futurVelocity = m_level.GetVelocityFromDist(distance + 2);
			Vector3 verticalVelocity = velocity;

			float z = Vector3.Dot(futurVelocity, transform.forward);
			float y = Vector3.Dot(futurVelocity, transform.up);

			float angle = Mathf.Atan2(y, z) * Mathf.Rad2Deg;

			return angle;
		}

		protected override void DoActionCrash()
        {
            base.DoActionCrash();
            leftSparkParticle.Stop();
            rightSparkParticle.Stop();
        }



        protected override void DoActionEnd()
        {
            base.DoActionEnd();

            float ratio;
            if (!IsUnstable)
            {

                if (m_unstableTime > 0)
                {
                    ratio = 1 - m_unstableTime / m_currentMovementDuration;
                    body.localRotation = Quaternion.Lerp(m_currentUnstableRotation, m_baseRotation, ratio);
                    m_unstableTime -= Time.deltaTime;
                }
                else m_unstableTime = 0;

                m_currentStableRotation = body.localRotation;
                m_stableTime = m_unstableTime;

            }
            else
            {

                if (m_unstableTime - m_stableTime < m_unstableMovementDuration - m_stableTime)
                {
                    ratio = (m_unstableTime - m_stableTime) / (m_unstableMovementDuration - m_stableTime);
                    body.localRotation = Quaternion.Lerp(m_currentStableRotation, m_unstableRotation, ratio);
                    m_unstableTime += Time.deltaTime;
                }
                else m_unstableTime = m_unstableMovementDuration;

                m_currentUnstableRotation = body.localRotation;
                m_currentMovementDuration = m_unstableTime;
            }
            Decelerate();
            
        }

		private void Accelerate()
		{
			m_speed += m_acceleration * Time.deltaTime;
			if (m_speed > m_maxSpeed)
				m_speed = m_maxSpeed;
		}

		private void Decelerate()
        {
			m_speed *= m_deceleration;
            if (m_speed < m_minSpeed)
				m_speed = m_minSpeed;
        }

		protected override void SetModeCrash(Vector3 direction)
		{
			base.SetModeCrash(direction);
            
            OnPlayerCrash?.Invoke();
			m_rigidbody.isKinematic = false;
			m_rigidbody.AddForce(direction, ForceMode.Impulse);
			m_rigidbody.AddTorque(Vector3.Cross(Vector3.up, direction) * m_crashTorqueWeight, ForceMode.Impulse);
		}
	}
}
