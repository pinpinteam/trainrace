﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class LevelManager : Singleton<LevelManager>
	{
		[SerializeField] private List<Level> levelList;

		[SerializeField] private Player m_playerPrefab;
		[SerializeField] private Rival m_rivalPrefab;

		public Player player;
		public Rival firstRival;
		public Rival secondRival;

		public Level actualLevel;
        private int currentLevelIndex;

		
		public void GenerateLevel(int levIndex)
		{
            if(levelList.Count == 0)
            {
                Debug.LogError("levelList not set !");
                return;
            }
            else if (levIndex >= levelList.Count) levIndex = 0;

            currentLevelIndex = levIndex;
			actualLevel = Instantiate(levelList[currentLevelIndex]);

			player = Instantiate(m_playerPrefab);
			player.Init(LevelManager.Instance.actualLevel.firstRoad);

			firstRival = Instantiate(m_rivalPrefab);
			firstRival.Init(LevelManager.Instance.actualLevel.secondRoad);
			firstRival.SetPlayerReference(player);

			secondRival = Instantiate(m_rivalPrefab);
			secondRival.Init(LevelManager.Instance.actualLevel.thirdRoad);
			secondRival.SetPlayerReference(player);

		}

        public void Next()
        {
            DestroyLevel();
            GenerateLevel(currentLevelIndex + 1);
        }

        public void Retry()
        {
            DestroyLevel();
            GenerateLevel(currentLevelIndex);
        }

        public void DestroyLevel()
        {
            Destroy(actualLevel.gameObject);
            Destroy(player.gameObject);
            Destroy(firstRival.gameObject);
            Destroy(secondRival.gameObject);
        }
	}
}
