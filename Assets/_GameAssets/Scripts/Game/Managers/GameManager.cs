﻿using System.Collections.Generic;
using UnityEngine;
using System.Numerics;
using Pinpin.Helpers;
using Pinpin.Scene.MainScene.UI;
using System;
using PaperPlaneTools;
using URandom = UnityEngine.Random;
using UVector2 = UnityEngine.Vector2;
using UVector3 = UnityEngine.Vector3;
using TMPro;
using Pinpin.UI;
using Cinemachine;
#if MADPINPIN
using MadboxAnalytics = MadBox.Services.ExternalServices.Analytics;
#endif

namespace Pinpin
{
    public class GameManager : Singleton<GameManager>
    {
        public static Action<int, float, int> OnUpdateProgression;
        public static Action<bool> OnLevelEnd;
        public bool IsLevelEnded { get; private set; } = false;
        [SerializeField] private MainPanel m_mainPanel;
        [SerializeField] GameObject m_money;
        [SerializeField] private Transform m_cameraTransform;
        // Timers
        private float m_vibrationTimer = 0f;

        [SerializeField] SpriteRenderer m_backGroundRenderer;

        private Coroutine m_limitedBoosterCoroutine;
        private bool m_mustShowLimitedBooster = false;
        private BigInteger m_currentLevelGoldEarned = 0;
        private string m_lastPatternName;
        [SerializeField] private int m_roundCount;
        private List<Round> m_rounds = new List<Round>();
        [SerializeField] private float m_distBetweenRounds = 10f;

        [SerializeField] private CinemachineVirtualCamera m_virtualCamera;
        [SerializeField] private CinemachineVirtualCamera m_virtualCrashCamera;
        [SerializeField] private float m_cameraFOVMultiplier = 0.5f;
        [SerializeField] private float m_cameraMaxFOV = 100;
        private float m_baseCameraFOV;
        

        private new void Awake ()
		{
			ApplicationManager.onRewardedVideoAvailabilityChange += OnRewardedVideoAvailabilityChange;
            m_baseCameraFOV = m_virtualCamera.m_Lens.FieldOfView;
			base.Awake();
		}

		// Use this for initialization
		void Start()
		{
			ComputeNeededXP();
			GenerateLevel();
		}

		public void ResetLevel ()
		{
			for (int i = 0; i < m_rounds.Count; i++)
			{
				Destroy(m_rounds[i].gameObject);
			}
			m_rounds.Clear();
			GenerateLevel();
		}

		private void GenerateLevel ()
		{
			OnUpdateProgression?.Invoke(0, 0f, m_roundCount);
			if (ApplicationManager.assets.rounds.Length < m_roundCount || m_roundCount == 0)
				return;
			float distance = 0f;
			List<int> roundIds;
			int fixedLevelCount = ApplicationManager.assets.rounds.Length / m_roundCount;
			if (ApplicationManager.datas.level < fixedLevelCount) // Remove if you don't want your rounds to be played in order in the first levels
			{
				roundIds = new List<int>();
				for (int i = 0; i < m_roundCount; i++)
				{
					roundIds.Add(i + ApplicationManager.datas.level * 4);
				}
			}
			else
			{
				roundIds = ApplicationManager.datas.lastLevel;
				if (roundIds.Count == 0)
				{
					for (int i = 0; i < m_roundCount; i++)
					{
						int nextID = -1;
						do
						{
							nextID = URandom.Range(0, ApplicationManager.assets.rounds.Length);
						} while (roundIds.Contains(nextID));

						roundIds.Add(nextID);
					}
					ApplicationManager.datas.SaveLevel();
				}
			}

			for (int i = 0; i < roundIds.Count; i++)
			{
				Round level = Instantiate(ApplicationManager.assets.rounds[roundIds[i]], new UVector3(0f, 0f, distance), UnityEngine.Quaternion.identity);
				m_rounds.Add(level);
				distance += m_distBetweenRounds;
			}

		}

		private void OnDestroy()
		{
            Player.OnPlayerFinish -= LevelComplete;
            Player.OnPlayerAccelerate -= SetCameraFOV;
            Rival.OnRivalFinish -= LevelFailed;
            WinPopup.OnClickPopup -= NextLevel;
            LosePopup.OnClickPopup -= RetryLevel;
        }

		private void ComputeNeededXP()
		{
            if (ApplicationManager.datas.level > 0)
                ApplicationManager.xpMax = MathHelper.Fibonnaci(ApplicationManager.datas.level) * ApplicationManager.config.game.baseXpToLevelUp + MathHelper.Fibonnaci(ApplicationManager.datas.level - 1) * ApplicationManager.config.game.baseXpToLevelUp;
            else
                ApplicationManager.xpMax = MathHelper.Fibonnaci(ApplicationManager.datas.level) * ApplicationManager.config.game.baseXpToLevelUp;
        }

		private void OnRewardedVideoAvailabilityChange ( bool value )
		{
			if (value && m_mustShowLimitedBooster)
			{
				m_mustShowLimitedBooster = false;
				m_mainPanel.DisplayLimitedBooster();
			}
		}
		
		

		private void Update()
		{
			if (Input.GetKeyDown(KeyCode.T))
				ApplicationManager.datas.haveCompleteTutorial = false;
			ApplicationManager.datas.UpdateBoostersTime(Time.deltaTime);
			if (m_vibrationTimer > 0)
				m_vibrationTimer -= Time.deltaTime;
		}
		

		public void AddCurrency(BigInteger currencyToAdd)
		{
			m_currentLevelGoldEarned += currencyToAdd;
			ApplicationManager.AddCoins(currencyToAdd);
		}
		public void AddMillion()
		{
#if CUSTOM
			AddCurrency(1000000);
#endif
		}

		public void DisplayLimitedBoosterManager ( float time )
		{
			if (m_limitedBoosterCoroutine != null)
			{
				StopCoroutine(m_limitedBoosterCoroutine);
			}
			m_limitedBoosterCoroutine = StartCoroutine(DisplayLimitedBoosterCoroutine(time));

		}

		private System.Collections.IEnumerator DisplayLimitedBoosterCoroutine ( float time )
		{
			yield return new WaitForSeconds(time);
#if MADPINPIN
			if (ApplicationManager.canWatchRewardedVideo)
			{
				m_mainPanel.DisplayLimitedBooster();
			}
			else
				m_mustShowLimitedBooster = true;
#endif
		}
		
        public void CashIn(BigInteger _value, UVector3 _position)
		{
			GameObject money = Instantiate(m_money);
			money.transform.position = new UVector3(_position.x, _position.y, 9);
			money.GetComponentInChildren<TextMeshPro>().text = "+ " + MathHelper.ConvertToEngineeringNotation(_value , 4);
			Destroy(money, 1.1f);
			AddCurrency(_value );
		}

		public void StartGame()
		{
#if MADPINPIN
			MadboxAnalytics.GameEvents.StartGame("normal", ApplicationManager.datas.level));
#endif

			OnUpdateProgression?.Invoke(0, 0f, m_roundCount);
			// TODO start game

			LevelManager.Instance.GenerateLevel(0);
            SetCamera();

            Player.OnPlayerFinish += LevelComplete;
			Player.OnPlayerCrash += LevelCrashed;
            Player.OnPlayerAccelerate += SetCameraFOV;
            Rival.OnRivalFinish += LevelFailed;
            WinPopup.OnClickPopup += NextLevel;
            LosePopup.OnClickPopup += RetryLevel;

			
		}

		private void LevelComplete ()
		{
            if (IsLevelEnded) return;
#if MADPINPIN
			MadboxAnalytics.GameEvents.EndGame(MadboxAnalytics.COMPLETION_STATUS.COMPLETE, (float)m_currentLevelGoldEarned, m_lastPatternName, 100);
#endif
			OnLevelEnd.Invoke(true);
            m_virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_BindingMode = CinemachineTransposer.BindingMode.WorldSpace;
            RateBox.Instance.IncrementCustomCounter();
			ApplicationManager.datas.xp -= ApplicationManager.xpMax;
			ApplicationManager.datas.lastLevel.Clear();
			ApplicationManager.datas.level++;
			ComputeNeededXP();
			m_currentLevelGoldEarned = 0;
            IsLevelEnded = true;
		}

        private void LevelFailed ()
		{
            if (IsLevelEnded) return;
#if MADPINPIN
			MadboxAnalytics.GameEvents.EndGame(MadboxAnalytics.COMPLETION_STATUS.FAIL, (float)m_currentLevelGoldEarned, m_lastPatternName);
#endif
            OnLevelEnd.Invoke(false);
            m_virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_BindingMode = CinemachineTransposer.BindingMode.WorldSpace;
            m_currentLevelGoldEarned = 0;
            IsLevelEnded = true;
        }

        private void LevelCrashed()
        {
            if (IsLevelEnded) return;
#if MADPINPIN
			MadboxAnalytics.GameEvents.EndGame(MadboxAnalytics.COMPLETION_STATUS.FAIL, (float)m_currentLevelGoldEarned, m_lastPatternName);
#endif
            m_virtualCamera.gameObject.SetActive(false);
            m_virtualCrashCamera.gameObject.SetActive(true);

            OnLevelEnd.Invoke(false);
            //Stop Camera
            m_currentLevelGoldEarned = 0;
            IsLevelEnded = true;
        }

        private void SetCameraFOV(float speed)
        {
            m_virtualCamera.m_Lens.FieldOfView = Mathf.Min(m_baseCameraFOV + speed * m_cameraFOVMultiplier, m_cameraMaxFOV);
        }

        private void NextLevel()
        {
            IsLevelEnded = false;
            LevelManager.Instance.Next();
            SetCamera();
        }

        private void RetryLevel()
        {
            IsLevelEnded = false;
            LevelManager.Instance.Retry();
            SetCamera();
        }

        private void SetCamera()
        {
            m_virtualCamera.gameObject.SetActive(true);
            m_virtualCrashCamera.gameObject.SetActive(false);
            m_virtualCamera.Follow = LevelManager.Instance.player.transform;
            m_virtualCrashCamera.Follow = LevelManager.Instance.player.transform;
            m_virtualCamera.LookAt = LevelManager.Instance.player.cameraTransform;
            m_virtualCamera.m_Lens.FieldOfView = m_baseCameraFOV;
            m_virtualCamera.GetCinemachineComponent<CinemachineTransposer>().m_BindingMode = CinemachineTransposer.BindingMode.LockToTargetNoRoll;
        }

	}
}
