﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class Character : MonoBehaviour
	{
		[SerializeField] protected float m_speed;
		[SerializeField] protected float m_maxSpeed;
		[SerializeField] protected float m_minSpeed;
		[SerializeField] protected float m_acceleration;
		[SerializeField, Range(0, 1)] protected float m_deceleration;

		protected BezierSpline m_level;
		public float distance { get; protected set; } = 0;
        protected bool IsCrashing { get; set; } = false;

        protected event Action DoAction;


		private void Awake()
		{
			SetModeVoid();
		}

		public virtual void Init(BezierSpline level)
		{
			m_level = level;
			ActualisePosition();
			SetModeNormal();
		}

		private void Update()
		{
			DoAction();
		}

		private void ActualiseTransform()
		{
			ActualiseRotation();
			ActualisePosition();
		}

		private void ActualisePosition()
		{
			transform.position = m_level.GetPointInWorldFromDist(distance);
		}

		private void ActualiseRotation()
		{
			float spineAngle = m_level.GetAngleFromDist(distance);
			Vector3 direction = m_level.GetDirectionFromDist(distance);
			Vector3 right = m_level.GetNormalFromDist(distance);
			right = Quaternion.AngleAxis(spineAngle, direction) * right;
			Vector3 up = Vector3.Cross(right, direction);
			transform.rotation = Quaternion.LookRotation(direction, up);
		}

		protected void SetModeVoid()
		{
			DoAction = DoActionVoid;
		}

		protected void DoActionVoid()
		{

		}

		protected void SetModeNormal()
		{
			DoAction = DoActionNormal;
		}

		protected virtual void DoActionNormal()
		{
			distance += m_speed * Time.deltaTime;
			ActualiseTransform();
		}

        protected void SetModeEnd()
        {
            DoAction = DoActionEnd;
        }

        protected virtual void DoActionEnd()
        {
            distance += m_speed * Time.deltaTime;
            ActualiseTransform();
        }

		protected virtual void SetModeCrash(Vector3 direction)
		{
			DoAction = DoActionCrash;
		}

		protected virtual void DoActionCrash()
		{
            IsCrashing = true;
		}
	}
}
