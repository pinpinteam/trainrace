﻿using Pinpin;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Pinpin
{
	public class Rival : Character
	{
        public static event Action OnRivalFinish;

        private float speedMultiplyHandicap = 0;
		public Player m_player;

		public void SetPlayerReference(Player player)
		{
			m_player = player;
		}

		protected override void DoActionNormal()
		{
			base.DoActionNormal();

            if (m_player.distance == 0) return;

			if (m_player.distance > distance)
			{
				Accelerate();
			}
			else Decelerate();

            if(distance >= m_level.TotalLength)
            {
                OnRivalFinish?.Invoke();
                distance = 0;
                SetModeEnd();
            }
        }

        protected override void DoActionEnd()
        {
            base.DoActionEnd();
            Decelerate();
        }

        private void Accelerate()
		{
			m_speed += m_acceleration * Time.deltaTime;
			if (m_speed > m_maxSpeed)
				m_speed = m_maxSpeed;
		}
		private void Decelerate()
		{
			m_speed *= m_deceleration;
			if (m_speed < m_minSpeed)
				m_speed = m_minSpeed;
		}

	}
}
