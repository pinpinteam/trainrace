﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LookAt : MonoBehaviour
{
	private Transform m_target;

	private void Start ()
	{
		m_target = Camera.main.transform;
	}

	// Update is called once per frame
	void Update()
    {
		transform.LookAt(m_target);
    }
}
